# Managed Versioning

The delivery group will create and manage all the stable branches and tags in
a component's repository. With Managed Versioning, new branches, tags,
and inclusion in the self-managed releases will be managed by the
Delivery group and versioning aligned with the [GitLab product
versioning scheme].

## Highlights

One of the way we follow our Product Principles at GitLab is by having
a [SaaS
First](https://about.gitlab.com/handbook/product/product-principles/#saas-first)
attitude.

Managed Versioning is how Delivery group allows development team to
focus on SaaS while still providing a solid and easy experience with
self-managed.

In a SaaS environment we can identify a component version just by its
SHA, because of that the component's team will no longer have to cut
releases in order to get included in product.

When release managers cut a release, every component following
Managed Versioning will be automatically released from its stable
branch, with proper tagging and changelog generation.

Every type of release will be simplified as the stable branches will
match those of the rails application, removing a layer of indirection
in finding what version of the component was included in a given GitLab
release, as well as bookkeeping the version number to avoid blocking
regular releases during a security release.

### What does the development team gain?

- No need to cut any release
- Automated changelog generation
- Automated stable branch generation
- Automated tagging
- Simplified patch release process
- Simplified security release process

### What does the development team lose?

- Control over the version scheme, it will follow the GitLab
  product version
- The ability to cut releases, only a release manager tagging
  a GitLab release will be able to cut a release for the component.
- All the chores related to cutting a release except bumping the
  version on `gitlab-org/gitlab` default branch.

## Process by release type

### auto-deploy

This is our most frequent release type, on an regular working day [we
tag about 8
packages](https://dashboards.gitlab.net/d/delivery-auto_deploy_packages/delivery-auto-deploy-packages-information?orgId=1&viewPanel=2)
and each one of those packages include the component's version
specified in its version file from the `gitlab-org/gitlab` repository.

A component's development team, will no longer need to cut a release
to upgrade the packaged version, they will skip all the chores
related to changelog generation, tagging, and stable branches
preparation and only bump the version file with the desired SHA.

Once the merge request is merged, the next auto_deploy branch
including that merge commit will make sure the we package that
component.

### monthly releases

A few days before the 22nd, the release managers cut the release candidate
release. This is the moment where we generate the stable branch from
the last successful production deployment.

If the candidate is passing QA and has no high severity and high
priority issues, it will be re-tagged as the regular monthly release
one working day before the 22nd.

### patch releases

Every time a release manager cuts a patch release, the components will
be tagged from the relevant stable branch. In order to have something
released as patch, the development team only needs to merge the fix on
the stable branch.

For high severity and high priority issues please reach out to the
release managers to discuss scheduling a patch release.

### security releases

Security releases are no different from patch releases in terms of
release automation. However, in order to allow AppSec to test the fix
in isolation, the development team will be requested to open the
standard set of merge request for `gitlab-org/security/gitlab` bumping
the version file to the SHAs of each fix (main branch and backports).

When the fix are approved, make sure you merge the component's merge
requests and assign the `gitlab-org/security/gitlab` ones to the bot
as usual.

## Requirements

In order to enroll in Managed Versioning the component must support
the following requirements:
- the current component's version must be lower than the current GitLab product version.
- it implements all the requirements from [the components page](../index.md)
- it implements the [company's changelog policy](https://docs.gitlab.com/charts/development/changelog.html)
- both CNG and Omnibus can build the component from a SHA

## Components under Managed Versioning

**Note: This section was updated on 2022-15-11**

Currently, releases for the following components are coordinated following
the Managed Versioning principles:

* [GitLab](https://gitlab.com/gitlab-org/gitlab)
* [Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab)
* [Gitaly](https://gitlab.com/gitlab-org/gitaly)
* [CNG](https://gitlab.com/gitlab-org/build/CNG)

### Versioning rules

We expect each component to conform with the following versioning rules:

| | Format | Example|
| ------  | ------- | ------- |
| Stable branch | `<major>-<minor>-stable` | `15-4-stable` |
| Tag | `v<major>.<minor>.<patch>` | `v15.4.5` |

However we have a list of [documented exceptions](components/versioning_rules_exceptions.md).

[GitLab product versioning scheme]: https://docs.gitlab.com/ee/policy/maintenance.html
